/**
 * @license create-automous

 * This source code is licensed and governed by a Proprietary License.
 * Permissions may be found in the LICENSE file located in the root directory.

 * © Automous (Pty) Ltd
 * https://automous.co.za | <connor@automous.co.za>
 *
*/

'use strict';

if (process.versions.node.split('.')[0] < 14) {
  console.error(
    'You are running Node ' +
      currentNodeVersion +
      '.\n' +
      'Concise Framework requires Node 14 or higher. \n' +
      'Please update your version of Node.'
  );
  process.exit(1);
}

const { init } = require('./bin/initChatbot');

init();
