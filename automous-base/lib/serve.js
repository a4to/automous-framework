/**
 * @license automous-base

 * This source code is licensed and governed by a Proprietary License.
 * Permissions may be found in the LICENSE file located in the root directory.

 * © Automous (Pty) Ltd
 * https://automous.co.za | <connor@automous.co.za>
 *
*/

const { exec, spawn } = require('child_process');
const path = require('path');
const port = 4050;
require('axioms.js')();
(async () => {
  try {
    log(`\n:g:[+] :x:Server running at :y:http://localhost:${port}:c:`);
    exec(`npx serve -l ${port} ${path.join(__dirname, '..', 'src')} >/dev/null 2>&1`, { stdio: 'inherit' }, (error, stdout, stderr) => {
      if (error) {
        console.error(`exec error: ${error}`);
        return;
      }
    });
    setTimeout(() => {
      process.env.BROWSER && exec(`${process.env.BROWSER} http://localhost:${port} >/dev/null 2>&1`, { stdio: 'inherit' });
    }, 1000);
  } catch (error) {
    console.error(error);
  }
})();
