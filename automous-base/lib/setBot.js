/**
 * @license automous-base

 * This source code is licensed and governed by a Proprietary License.
 * Permissions may be found in the LICENSE file located in the root directory.

 * © Automous (Pty) Ltd
 * https://automous.co.za | <connor@automous.co.za>
 *
*/

const prompts = require('prompts');
const axios = require('axios');
const path = require('path');
const os = require('os');
const fs = require('fs');
const login = require('./login');
require('axioms.js')();
let response;

const getBots = async (token) => {
  const {data} = await axios.post('https://automous.co.za/api/auth/getBots', {token});
  if (data.success) {
    return data.bots.map(bot => {
      return {
        title: bot.name,
        value: bot.id,
        description: bot.id
      }
    });
  } else {
    log('\n:r:[-] :y:Failed to retrieve bots! Please re-authenticate...\n');
    let res = await login(false);
    if(res) return getBots(res);
    return false;
  }
}

const ask = async (choices) => {
  let questions = [
    { type: 'select', name: 'bot', message: 'Select a Bot:', choices: choices, initial: 0, limit: 50, }
  ];
  log('\n:y:[?] :x:Please Select a Bot:\n');
  const response = await prompts(questions);
  if(!response || response.bot === undefined) {
    log('\n:r:[-] :y:No bot selected! Exiting...\n');
    process.exit(1);
  }
  return response;
}

const readConfig = async () => {
  const configFile = path.join(os.homedir(), '.automousrc');
  if (!fs.existsSync(configFile)) return false;
  return JSON.parse(await fs.readFileSync(configFile, 'utf8'));
}

const setCdn = async (newToken) => {
  const index = path.join(__dirname, '..', 'src', 'index.html');
  const regex = /data-token src="(.*?)?"/g;
  let content = await fs.readFileSync(index, 'utf8');
  content = content.replace(regex, `data-token src="https://automous.co.za/cdn/cb/${newToken}"`);
  await fs.writeFileSync(index, content);
}

const main = async () => {
  try {
    let config = await readConfig();
    if (!config) {
      //log('\n:r:[-] :y:No configuration found! Please login to Automous...\n');
      log('\n:y:[?] :x:Please login to Automous:\n');
      let res = await login(false);
      if(res) config = await readConfig();
      else {
        log('\n:r:[-] :y:Failed to authenticate! Please try again...\n');
        process.exit(1);
      }
    }
    let bots = await getBots(config.token);
    if (!bots) return false;
    let response = await ask(bots);
    await setCdn(response.bot);
    log('\n:g:[+] :x:Bot set successfully!:c:');
  } catch (e) {
    log('\n:r:[-] :y:An error occurred! Please try again...\n');
    log(e);
  }
}

main();

